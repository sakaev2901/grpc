package com.example.service2;

import com.example.grpc.Request;
import com.example.grpc.ServiceGrpc;
import com.example.grpc.Test;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Iterator;
import java.util.List;

@SpringBootApplication
public class Service2Application implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Service2Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        ManagedChannel channel = ManagedChannelBuilder
                .forTarget("localhost:8090")
                .usePlaintext()
                .build();

        ServiceGrpc.ServiceBlockingStub serviceBlockingStub = ServiceGrpc.newBlockingStub(channel);
        Request request = Request.newBuilder()
                .setAge(21)
                .setName("Eldar")
                .addAllLanguages(List.of("english", "tatar", "java"))
                .build();
        serviceBlockingStub.execute(request).forEachRemaining(System.out::println);
        channel.shutdownNow();
    }
}


