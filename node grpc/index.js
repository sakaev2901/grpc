const grpc = require('grpc')
const PROTO_PATH = './test.proto'
const protoLoader = require('@grpc/proto-loader');


var packageDefinition = protoLoader.loadSync(PROTO_PATH, {});

let Service = grpc.loadPackageDefinition(packageDefinition).com.example.grpc.Service

const client = new Service(
    'localhost:8090',
    grpc.credentials.createInsecure()
)

client.execute({name: 'Eldar', age: 21, languages: ['java', 'node']}, (err, data) => {
    if (err) console.log(err);
    else console.log(data);
})


