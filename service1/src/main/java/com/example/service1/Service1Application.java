package com.example.service1;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Service1Application implements CommandLineRunner {
    private static final Integer PORT = 8090;

    @Autowired
    private GreetingServiceImpl greetingService;

    public static void main(String[] args) {
        SpringApplication.run(Service1Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        Server server = ServerBuilder
                .forPort(PORT)
                .addService(greetingService)
                .build();
        server.start();
        System.out.println("Server running on port: " + PORT);
        server.awaitTermination();
    }
}
