package com.example.service1;

import com.example.grpc.Request;
import com.example.grpc.Response;
import com.example.grpc.ServiceGrpc;
import com.example.grpc.Test;
import io.grpc.stub.StreamObserver;
import org.springframework.stereotype.Component;

@Component
public class GreetingServiceImpl extends ServiceGrpc.ServiceImplBase {
    @Override
    public void execute(Request request, StreamObserver<Response> responseObserver) {
        System.out.println(request);
        var s = String.format("Hello, %s. Your age is %d and you know %s languages",
                request.getName(),
                request.getAge(),
                String.join(", ", request.getLanguagesList())
                );
        for (int i = 0; i < 10; i++) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                throw new IllegalStateException(e);
            }
            responseObserver.onNext(Response.newBuilder()
                    .setText(s)
                    .build());
        }
//        responseObserver.onError(new StatusException(Status.INVALID_ARGUMENT.withDescription("F##KING ERROR")));
        responseObserver.onCompleted();
    }


}
//